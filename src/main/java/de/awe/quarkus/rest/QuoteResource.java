package de.awe.quarkus.rest;

import javax.inject.Inject;
import javax.json.Json;
import javax.json.JsonObjectBuilder;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import de.awe.quarkus.model.Quote;
import de.awe.quarkus.service.QuoteService;

/**
 * The quote ressource.
 * 
 * @author andreas.wenskus@mail.de
 */
@Path(QuoteResource.URI)
public class QuoteResource {

	public static final String URI  = "/quote";
	
    @Inject
    QuoteService quoteService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String index() {
        Quote quote = quoteService.getRandomQuote();
        JsonObjectBuilder response = Json.createObjectBuilder();
        response.add("author", quote.getAuthor());
        response.add("content", quote.getContent());
        return response.build().toString();
    }
}
