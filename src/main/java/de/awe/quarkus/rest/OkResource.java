package de.awe.quarkus.rest;

import javax.json.Json;
import javax.json.JsonObjectBuilder;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/_status")
public class OkResource {

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String index() {
        JsonObjectBuilder response = Json.createObjectBuilder();
        response.add("ok", true);
        return response.build().toString();
    }
}
