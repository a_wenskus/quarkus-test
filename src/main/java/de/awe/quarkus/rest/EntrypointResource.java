package de.awe.quarkus.rest;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path(EntrypointResource.URI)
public class EntrypointResource {

	public static final String URI  = "/";

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String index() {
        return "Hello World";
    }
}
