package de.awe.quarkus.rest;

import javax.json.Json;
import javax.json.JsonObjectBuilder;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/_meta")
public class MetaResource {

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String index() {
        JsonObjectBuilder response = Json.createObjectBuilder();
        response.add("name", "quarkus-test");
        response.add("version", "0.0.1");
        return response.build().toString();
    }
}
