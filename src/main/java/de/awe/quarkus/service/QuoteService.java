package de.awe.quarkus.service;

import java.util.Collection;
import java.util.Set;

import de.awe.quarkus.model.Quote;

/**
 * The quote service public interface.
 * 
 * @author andreas.wenskus@mail.de
 *
 */
public interface QuoteService {
	
	/**
	 * @return a random quote from quotes database
	 */
	Quote getRandomQuote();
	
	/**
	 * @return all quotes from quotes database
	 */
	Collection<Quote> getAllQuotes();

	/**
	 * @param pAuthor, the quote author 
	 * @return set of quotes of a author from quotes database
	 */
	Set<Quote> getQuotesByAuthor(String pAuthor);
}
