package de.awe.quarkus.service;

import java.util.Collection;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import javax.enterprise.context.ApplicationScoped;

import de.awe.quarkus.model.Quote;

/**
 * The quote service implementation.
 * 
 * @author andreas.wenskus@mail.de
 */
@ApplicationScoped
public class QuoteServiceImpl implements QuoteService {

	private static final Logger log = Logger.getLogger(QuoteServiceImpl.class.getName()) ;
    		
	private Set<Quote> quotes = new HashSet<>();
	
    public QuoteServiceImpl() {
    	addQuote("Heinz Rühmann", "Man kann auch ohne Hund leben, aber es lohnt sich nicht.");
        addQuote("Heinz Rühmann", "Schon manche Gesundheit ist dadurch ruiniert worden, dass man auf die der anderen getrunken hat.");
    	addQuote("Heinz Rühmann", "Wer morgens zerknittert aufsteht, hat den ganzen Tag über viele Entfaltungsmöglichkeiten.");
    	addQuote("Wilhelm Busch","Dumme Gedanken hat jeder, aber der Weise verschweigt sie.");
        addQuote("Charles Bukowski","Das Problem dieser Welt ist, dass die intelligenten Menschen so voller Selbstzweifel und die Dummen so voller Selbstvertrauen sind.");
    	addQuote("Konfuzius", "Am Rausch ist nicht der Wein schuld, sondern der Trinker.");
        addQuote("Homer Simpson","Alles ist in bester Ordnung – solange ich genug Bier im Kühlschrank habe!");    	
        addQuote("minim","magna consec");    	
    }

    private Quote addQuote(String pAuthor, String pContent) {
    	Quote quote = Quote.of(pAuthor, pContent);
    	quotes.add(quote);
    	log.info("quote added: " + quote.toString());
    	return quote;
    }
    
    @Override
    public Quote getRandomQuote() {
    	Quote[] quotes = getAllQuotes().toArray(new Quote[0]);
        return quotes[new Random().nextInt(quotes.length)];
    }
   
    @Override
    public Collection<Quote> getAllQuotes() {
    	return this.quotes;
    }
    
    @Override
    public Set<Quote> getQuotesByAuthor(String pAuthor) {
    	assert(pAuthor != null);
    	Set<Quote> r = this.quotes.stream().filter((quote) -> quote.getAuthor().equalsIgnoreCase(pAuthor)).collect(Collectors.toSet());
    	return this.quotes.stream().filter((quote) -> quote.getAuthor().equalsIgnoreCase(pAuthor)).collect(Collectors.toSet());
    }
    
}