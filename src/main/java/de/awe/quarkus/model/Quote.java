package de.awe.quarkus.model;

import java.util.Objects;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * A quote object represent one quote.  
 *  
 * @author Andreas Wenskus
 */
public class Quote {
	
	/**
	 * The quote author
	 */
	private String author;
	
	/**
	 * The quote content
	 */
	private String content;
	
	/**
	 *  static quote constructor
	 *  @param pAuthor, quote author
	 *  @param pContent, quote content
	 *  @return the created quote
	 */
	public static Quote of(@Size(min = 5) String pAuthor, @Size(min = 5) String pContent) {
		return new Quote(pAuthor, pContent); 
	}
	
	private Quote() { 	
	}
	
	private Quote(@Size(min = 5) String pAuthor, @Size(min = 5) String pContent)  {
		assert (pAuthor != null && !pAuthor.isBlank());
		assert (pContent != null && !pContent.isBlank());
		this.author = pAuthor;
		this.content = pContent;
	}

	/**
	 * @return the quote author
	 */
	public String getAuthor() {
		return author;
	}

	/**
	 * @param pAuthor, the quote author to set
	 */
	public void setAuthor(@NotNull String pAuthor) {
		this.author = pAuthor;
	}

	/**
	 * @return the quote content
	 */
	public String getContent() {
		return content;
	}

	/**
	 * @param pContent the quote content to set
	 */
	public void setContent(String pContent) {
		this.content = pContent;
	}

	/**
	 * The hash code of an Quote.
	 * @return The hash code of this Quote.
	 */
	@Override
	public int hashCode() {
		return Objects.hash(author, content);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!(obj instanceof Quote))
			return false;
		Quote other = (Quote) obj;
		return Objects.equals(author, other.author) && Objects.equals(content, other.content);
	}

	/**
	 * Returns a string representation of this Quote, mainly for display in log files etc.
	 * @return A string
	 */
	@Override
	public String toString() {
		return "Quote [author=" + author + ", content=" + content + "]";
	}	
	
}