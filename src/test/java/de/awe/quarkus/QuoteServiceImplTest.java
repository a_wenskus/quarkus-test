package de.awe.quarkus;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.junit.jupiter.api.Test;

import de.awe.quarkus.model.Quote;
import de.awe.quarkus.service.QuoteService;
import de.awe.quarkus.service.QuoteServiceImpl;

public class QuoteServiceImplTest {
	
	private final QuoteService quoteService = new QuoteServiceImpl();
	
	public QuoteServiceImplTest() {
	}
	
    @Test
    public void testRandomQuote() {
    	Quote randomQuote = quoteService.getRandomQuote();
    	assertTrue(quoteService.getAllQuotes().contains(randomQuote));
    }

    @Test
    public void testAllQuotes() {
    	assertTrue(quoteService.getAllQuotes().size() > 0);
    	for (Quote quote : quoteService.getAllQuotes()) {
    		assertTrue(quote.getAuthor() != null);
    	}
    }

    @Test
    public void testQuotesByAuthor() {
    	assertTrue(quoteService.getQuotesByAuthor("_notExists_").size() == 0);
    	Collection<Quote> allQuotes = quoteService.getAllQuotes();
    	Map<String, Integer> author2quoteCountMap = new HashMap<>();
    	for (Quote quote : allQuotes) {
    		String author = quote.getAuthor();
    		Integer quoteCount = author2quoteCountMap.get(author);
    		author2quoteCountMap.put(author, quoteCount == null ? Integer.valueOf(1) : quoteCount + 1);  		
    	}
    	for (Map.Entry<String, Integer> author2quoteCount : author2quoteCountMap.entrySet()) {
    		String author = author2quoteCount.getKey();
    		Set<Quote> quotesByAuthor = quoteService.getQuotesByAuthor(author);
        	assertTrue(author2quoteCount.getValue().intValue() == quotesByAuthor.size());
    	}
    }
    
}
