package de.awe.quarkus;

import static io.restassured.RestAssured.given;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import javax.inject.Inject;

import org.junit.Test;

import de.awe.quarkus.model.Quote;
import de.awe.quarkus.rest.QuoteResource;
import de.awe.quarkus.service.QuoteService;
import io.quarkus.test.common.http.TestHTTPEndpoint;
import io.quarkus.test.junit.QuarkusTest;
import io.restassured.response.ExtractableResponse;
import io.restassured.response.ValidatableResponse;

@TestHTTPEndpoint(QuoteResource.class)
@QuarkusTest
public class QuoteResourceTest {
	
	@Inject
	QuoteService quoteService;
	
    @Test
    public void testEndpoint() {    			
    	ValidatableResponse response = given().when().get().then();
    	response.statusCode(200);
    	
        ExtractableResponse resp = response.extract();
        Quote q = resp.as(Quote.class);    
        assertNotNull(q);
		assertTrue(quoteService.getAllQuotes().contains(q));
    }
   
}
