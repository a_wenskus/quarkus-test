package de.awe.quarkus;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.is;

import org.junit.Test;

import de.awe.quarkus.rest.EntrypointResource;
import io.quarkus.test.junit.QuarkusTest;

@QuarkusTest
public class ExampleResourceTest {

    @Test
    public void testHelloEndpoint() {
        given()
        		.when().get(EntrypointResource.URI)
                .then()
                .statusCode(200)
                .body(is("Hello World"));
    }

}
